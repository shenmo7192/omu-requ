#include "widget.h"
#include <QDebug>
#include <QDir>
Widget::Widget(QWidget *parent) : QWidget(parent)
{
    QDesktopWidget* pDesktopWidget = QApplication::desktop();
    QRect screenRect = QApplication::desktop()->screenGeometry();
    qaq = new QSettings(QDir::currentPath()+"/config.bunny", QSettings::IniFormat);
    TopLeft->setShell(qaq->value("TopLeftShell").toString());
    TopRight->setShell(qaq->value("TopRightShell").toString());
    LowerLeft->setShell(qaq->value("LowerLeftShell").toString());
    LowerRight->setShell(qaq->value("LowerRightShell").toString());

    TopLeft->setGeometry(0,0,1,1);
    TopRight->setGeometry(screenRect.width()-1,0,1,1);
    LowerLeft->setGeometry(0,screenRect.height()-1,1,1);
    LowerRight->setGeometry(screenRect.width()-1,screenRect.height()-1,1,1);
    TopLeft->show();
    TopRight->show();
    LowerLeft->show();
    LowerRight->show();
}
